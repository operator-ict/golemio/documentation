# Golemio - dokumentace modulů Datové platformy

Dokumentace je dostupná na https://operator-ict.gitlab.io/golemio/documentation

# Lokální spuštění

## Prerequisity

- Jekyll (4.3)
- Ruby (>3.0)

## Instalace
```
bundle install
```

## Spuštění

```
bundle exec jekyll serve
```


## Docker develepment

```
docker run --rm \
  --volume="$PWD:/srv/jekyll" \
  --volume="$PWD/vendor/bundle:/usr/local/bundle" \
  -p 4000:4000 \
  -it jekyll/jekyll:4.2.2 \
  jekyll serve
```