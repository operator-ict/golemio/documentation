---
layout: default
title: Open data & API
nav_order: 8
description: "Information regarding published open data and public API."
lang-ref: open-data-api
lang: en
---

# Open data & API

> The project provides the general public with data that can be used further as Open Data. These data are real-time data, statistical data or code lists, catalogues, and map data.

## Access options

- The open data catalogue of the city of Prague ([opendata.praha.eu](http://opendata.praha.eu){:target="_blank"})
    - Exports of data with meta-information about the structure, nature, source, update frequency, contact person, etc.
    - A permanent URL for data sets
    - Cataloguing within NKOD ([data.gov.cz](//data.gov.cz){:target="_blank"})
- Public API – Access in real-time to the Golemio data platform
    - REST API documented on [api.golemio.cz/docs/public-openapi/](https://api.golemio.cz/docs/public-openapi/){:target="_blank"}
    - Access secured with the help of `JWT`


## Published data in the OD catalogue

Operator ICT runs a catalogue of open data for the city of Prague, the data published in the Golemio project can be found on [opendata.praha.eu/organizations/operator-ict](https://opendata.praha.eu/organizations/operator-ict){:target="_blank"}.

## Access to the public API

### API Documentation

> [api.golemio.cz/docs/public-openapi/](https://api.golemio.cz/docs/public-openapi/){:target="_blank"}

<br >

At this link you can find the complete documentation of our REST API, including the description of individual endpoints and methods, exemplary requests, description of the structure and examples of returning data, request headers and the option of a testing (mock/sandbox) server or generating a code for an API call in various languages (cURL, Node.js, Java, Python,…).

### API URL

> https://api.golemio.cz/`version`/`route`


#### Security

We use a JWT token / a generated API key.

### User registration

On the page [api.golemio.cz/api-keys](//api.golemio.cz/api-keys){:target="_blank"} you can register for free and after an email verification, you can generate an api-key for accessing the API.

<!-- ## Documentation of datasets

For completeness, we state here also some of the datasets, where its preferred to extent the documentation beyond the framework of documentation in API Blueprint.

By clicking on the respective subcategory, you can go to the detailed description. -->
