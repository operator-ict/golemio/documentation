---
layout: default
title: Architecture
nav_order: 2
description: "Documentation of platform architecture"
lang-ref: architecture
lang: en
has_children: true
---

# Platform architecture

<img src="{{ site.baseurl }}/assets/images/schemas/DP_schema_general.svg" alt="Schéma architektury" class="img-highlighted"/>

The following table contains a list of all the information that describes the architecture and the design of the platform from all points of view.

## List of documentation

| Name | Description |
|:-|:-|
| [System architecture]({% link en/architecture/system-architecture.md %}) |A description of the overall application architecture with the link to subsequent systems. The documentation contains schemes for individual areas in the extent of standard architectonic documentation. |
| [Programming documentation]({% link en/architecture/programming-documentation.md %}) | Used classes, interfaces, structures and modules of the application with a description of the functionality and with the option of using this documentation for the purpose of the current functionality during extension of the application. |
