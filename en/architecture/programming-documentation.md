---
layout: default
title: Programming documentation
nav_order: 8
description: "Links to repositories, description of contribution."
lang-ref: programming-documentation
lang: en
parent: Architecture
---

# Programming documentation

The central repository of the project can be found on [gitlab.com/operator-ict/golemio/](//gitlab.com/operator-ict/golemio/)

All the repositories with codes automatically generate SW documentation to their classes, functions, interfaces. We use [TypeDoc](//typedoc.org/) for generating this programming documentation.


### Links to programming documentation of all the modules

* [Input Gateway](//operator-ict.gitlab.io/golemio/code/input-gateway/),
* [Output Gateway](//operator-ict.gitlab.io/golemio/code/output-gateway/),
* [Integration Engine](//operator-ict.gitlab.io/golemio/code/integration-engine/),
* [CRON](//operator-ict.gitlab.io/golemio/code/cron-tasks/),
* [Validator](//operator-ict.gitlab.io/golemio/code/validator/),
* [Utils](//operator-ict.gitlab.io/golemio/code/utils/),
* [Errors](//operator-ict.gitlab.io/golemio/code/errors/).

## The repository of this documentation

This documentation can be found on [gitlab.com/operator-ict/golemio/documentation](//gitlab.com/operator-ict/golemio/documentation)

## Code Contribution

Do not hesitate to contribute to the projects or any of the documentation. Regarding the code, please follow the [contribution guidelines](https://gitlab.com/operator-ict/golemio/code/output-gateway/blob/development/CONTRIBUTING.md). Propose new changes via [merge requests](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).
