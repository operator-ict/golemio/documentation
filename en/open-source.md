---
layout: default
title: Open Source
nav_order: 9
description: "Publishing of source codes"
lang-ref: open-source
lang: en
---

# Open Source

> We publish the source code of the whole project as an Open Source under the license of MIT.

## Git

You can find the project’s central group of repositories on [gitlab.com/operator-ict/golemio/](//gitlab.com/operator-ict/golemio/). These repositories also serve as a primary source for us as an operating environment of the Golemio data platform. What we publish, we use ourselves. If you decide to contribute to the project, you can create a merge request in the right format with the proposed change, and after its approval and acceptance to the repository, the change will automatically be deployed into the (even into the production) environment of the Golemio Data platform. However, do not forget to read the [contribution guidelines](https://gitlab.com/operator-ict/golemio/code/modules/core/-/blob/master/CONTRIBUTING.md?ref_type=heads).

We ❤ OpenSource and we use it a lot, we depend on a number of packages that are available for free (especially from the [npmjs.com](https://www.npmjs.com/)) ecosystem. Within the project we even publish some packages directly to NPMjs (for our simple use and eventually for other projects). You can find all our published packages under the scope [@golemio](https://www.npmjs.com/search?q=%40golemio).
<br>
