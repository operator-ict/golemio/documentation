---
layout: default
title: FAQ
nav_order: 6
description: "Frequently asked questions for Data Platform Golemio."
lang-ref: faq
lang: en
---

# FAQ

> This is a set of the most asked questions regarding the data platform, if this article does not answer your questions, contact us at  [golemio@operatorict.cz](mailto:golemio@operatorict.cz)


## General

### Does your solution fulfill  the function of an IoT platform in collecting data from SigFox, Lora and other networks?

No. In our projects in Prague, we have so far encountered suppliers which provided the standard REST API through HTTP, so it is not planned to use it. However thanks to the modular architecture of the platform it is possible to preorder an existing solution providing the function of an IoT platform for communication on corresponding IoT networks.

### When will the whole Data platform become an Open Source?

In half of the year 2019, we have gradually started to publish source codes by individual modules. During the year 2019 it is planned to publish the complete set of modules for the platform.

### Is there a software which is prepared to be deployed in other cities?

In the first phase, we will publish modules in the way that they were designed and are being used in Prague. In the second phase (the year 2020) we are open to generalizing the solution and enabling the deployment of the software in other cities. It should not be difficult, we have already designed this solution with this aim.

### Is it free?

We have published the codes under an MIT license, so the answer is yes, you can use the codes even for commercial purposes for free within the conditions of this free license. If you would like to read more about MIT, we recommend the article on [choosealicense.com/licenses/mit/](//choosealicense.com/licenses/mit/).

### Is somebody actually using it?
Yes, of course, Prague is currently using this solution for the integration of municipal data.



## Architecture

### Your orchestration is through Docker Swarm. Have you considered Kubernetes?

Yes, it is in our plan as a target, however so far we did not have the time or capacity to do it. And (an easier) Swarm is, for now, sufficient for us.

### You use stateless server applications. Where do you keep the state?

In the database/message broker.

### Does it mean that various modules are dependent on the same database and they are not individual and independent microservices?

Yes. We have a common database, at least for the output-gateway, integration-engine. The permission-proxy and user data are stored in an individual and separated relational database.

### Why are the applications separated?

Mainly due to scalability and individual deployment. If we change something in the API output, then we will deploy only new output-gateways. We can also scale individually, e.g. a layer for accepting data (input-gateway), where we can deploy e.g. ten times more instances independently if we start to receive a bigger amount of data.

### Permission-proxy – why did you not use a finished API gateway solution, which would solve access rights?

We have thought about it for a long time, however, our functional requirements did not cover any available solution (specific cases of rights allocation e.g. directly to particular data in the database, not just on an API URL). However, permission-proxy is still replaceable with a different security layer, if these specific requirements are not needed.

### If more modules are dependent on one database, then how do you deploy new versions?

We deploy smaller (minor and patch) versions continually by individual modules without an outage. If the new version does not change the structure of the current data in the database or does not retroactively deploy an incompatible change, there is no problem (adding new data, integration of a new set of data, new functionality, bugfixes). If it is a (major) version which ‘breaks’ the previous structure of the database, we have set a [process of deployment and version promotion](https://gitlab.com/operator-ict/golemio/devops/infrastructure/blob/master/docs/major-release.md). The API is then available under a new URL with a suffix of e.g. `/v2/`.
