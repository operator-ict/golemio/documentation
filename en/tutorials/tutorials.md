---
layout: default
title: Tutorials
nav_order: 7
description: "Tutorials for using the Data Platform."
has_children: true
lang-ref: tutorials
lang: en
---

# Tutorials

Here you can find a collection of all the tutorials for configuration of existing datasets or integration of new ones, tutorials for launching the project and other general tutorials. If you have any questions, email us at [golemio@operatorict.cz](mailto:golemio@operatorict.cz)
