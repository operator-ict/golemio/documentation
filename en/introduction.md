---
layout: default
title: Introduction
nav_order:
description: "Documentation of Data platform."
has_children: true
lang-ref: introduction
lang: en
---

# General documentation of the Data Platform

> A data platform is an application set for providing services related to data of the city – gathering data from various sources and providers (municipal companies, external systems, OpenData, static data), processing, transformation and consolidation of data, data analysis and the subsequent providing of an output and a presentation of the data to its clients in the form of data analyses, visualization, publishing of open data or providing an open realtime API.

## The basic functional scheme

<img src="{{ site.baseurl }}/assets/images/schemas/GolemioDataInput_en.svg" alt="Vstupy Datové platformy" class="img-highlighted"/>

## Who does the platform serve?

* Client  - **Prague City Hall and municipal districts**
    * Tailored analytic web applications
    * Creation of public data outputs
    * BI reporting, interactive analyses, and documentation for decision-making
    * Monitoring and alerting on the basis of real-time data
* Client  - **Municipal companies**
    * BI reporting, creation of analyses and documentations
    * Realization of an integrative interlayer between various IT systems
* Client  - **Commercial entities**
    * Exchange of data that allows better decision-making for the city
    * Open cooperation that improves municipal services
* Client  - **The public**
    * Public web applications
    * Opendata
    * Public API for integration into third-party applications

## What is the platform able to process?

* Transport data:
    * Location of public transport vehicles, real-time calculation of delays
    * Transport schedules
    * Detectors of the passage of cars
    * Data regarding the current traffic situation (congestion, accidents, lockouts)
    * Real-time parking situation (P+R, on-street parking in paid zones, commercial parking, parking for drivers with severe health disability)
* Air
    * Pollutants, temperature, humidity, the index of quality and other sensor data from weather stations
    * Smart City elements with sensors of the urban environment
* Motion
    * Counters of passages of pedestrians
    * Bike counters (detectors of passages, mobile bike navigation)
* BI analyses
    * Analysis of the impact of Airbnb and other platforms
    * Processing of map documentation (paid parking zones, catalogues of services)
    * Potential of green roofs in Prague
    * Analysis of the behaviour of Lítačka card users
* IoT devices (through API suppliers)
    * Sensors monitoring the fullness of waste containers
    * Smart lighting
    * Quality of the environment in buildings
    * Energy monitoring of buildings
    * E-Health project

## The platform in Prague is constituted mostly by a team of experts – consultants and analysts, the SW is only their tool

The aim of this project is to create a united platform for data from the Smart City area, which is the area of sensor data and data from new technologies and applications. We do not want to be a data graveyard for all the data of all the municipal IT systems which would end without further purpose and utilization.

A team of consultants and analysts of the Golemio project provides Prague (the city hall, municipal districts, and companies) with the service of data analysis and development. More can be found on [www.golemio.cz/en](https://golemio.cz/en)

## Contribute

Do not hesitate to contribute to projects or any of the documentation (this documentation is available on  [gitlab.com/operator-ict/golemio/documentation](//gitlab.com/operator-ict/golemio/documentation),we will be glad if you will help us with its improvement).

You can propose new changes via [merge requests](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

Regarding the code, please follow the [contribution guidelines](https://gitlab.com/operator-ict/golemio/code/output-gateway/blob/development/CONTRIBUTING.md).

## The author of the Golemio Data platform

The software of the Golemio Data platform was created by the company Operátor ICT a.s. in close cooperation of the Data City Lab Department (formerly known as the Department of the Data Platform) and the Department of Development and Analysis.

<br>

<img src="{{ site.baseurl }}/assets/images/logos/OICT_logo.png" alt="Logo Operator ICT" style="margin-botom: 1rem; height: 90px;"/>

<img src="{{ site.baseurl }}/assets/images/logos/logo_praha.svg" alt="Logo Praha" style="margin-botom: 1rem; height: 90px"/>
