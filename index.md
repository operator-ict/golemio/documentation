---
layout: default
title: Úvod
nav_order: 1
description: "Dokumentace Datové platformy."
lang-ref: introduction
lang: cs
redirect_from:
  - /
---

# Dokumentace Datové platformy Golemio

> Datová platforma je aplikační set pro poskytování sady služeb nad městskými daty - shromáždění dat z řady zdrojů a poskytovatelů (městské společnosti, externí systémy, OpenData, statická data), zpracování, transformace a konsolidace dat, datová analýza a následné poskytnutí výstupů a prezentace dat svým klientům, jak ve formě datových analýz, vizualizací, publikování OpenDat, nebo poskytování otevřeného realtime API.

## Komu platforma slouží?

* Klient - **Magistrát a městské části**
    * Analytické webové aplikace na míru
    * Tvorba veřejných výstupů dat
    * BI reporting, interaktivní analýzy a podklady pro rozhodování
    * Monitoring a alerting na základě realtime dat
* Klient - **Městské společnosti**
    * BI reporting, tvorba analýz a podkladů
    * Realizace integrační mezivrtstvy mezi různými IT systémy
* Klient - **Komerční subjekty**
    * Výměna dat sloužící městu pro lepší rozhodování
    * Otevřená spolupráce zlepšující městské služby
* Klient - **Veřejnost**
    * Veřejné webové aplikace
    * Opendata
    * Public API pro integraci do aplikací třetích stran

## Co umí platforma zpracovat?

* Dopravní data:
    * polohy vozů MHD, realtime výpočet zpoždění
    * jízdní řády,
    * detektory průjezdů aut,
    * data o aktuální dopravní situaci (kongesce, nehody, výluky)
    * realtime parkovací situace (P+R, on-street v zonách placeného stání, komerční parkoviště, stání pro ZTP řidiče)
    * cyklosčítače (detektory průjezdů, mobilní cyklonavigace)
* BI analýzy
    * analýza dopadu Airbnb, popř. jiných platforem
    * zpracování mapových podkladů (zóny placeného stání, katalogy služeb)
    * analýza chování uživatelů karty Lítačka
* IoT zařízení (skrze API dodavatele)
    * senzory zaplněnosti odpadových nádob
    * energetický monitoring budov
    
## Platformu v Praze tvoří především tým odborníků - konzultantů, produktáků, datových analytiků a vývojářů, kde SW je pouze jejich nástroj.

Cílem projektu je vytvořit jednotnou platformu pro data z tzv. Smart City oblasti, tedy oblasti senzorických dat, dat z nových technologií a aplikací.
Nechceme být datovým hrobem pro všechna data ze všech IT systémů města bez dalšího účelu a využití.

Více na [golemio.cz](https://golemio.cz)

## Přispějte

Neváhejte přispět do projektů nebo jakékoliv dokumentace (tato dokumentace je dostupná na [gitlab.com/operator-ict/golemio/documentation](//gitlab.com/operator-ict/golemio/documentation), budeme rádi když nám ji pomůžete vylepšit).

Nové změny navrhujte přes [merge requesty](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

Co se týče kódu, dodržujte prosím [contribution guidelines](https://gitlab.com/operator-ict/golemio/code/output-gateway/blob/development/CONTRIBUTING.md).

## Autor Datové platformy Golemio

Software Datové platformy Golemio byl vytvořen společností Operátor ICT a.s..

<br />

<img src="{{ site.baseurl }}/assets/images/logos/OICT_logo.png" alt="Logo Operator ICT" style="margin-botom: 1rem; height: 90px;"/>

<img src="{{ site.baseurl }}/assets/images/logos/logo_praha.svg" alt="Logo Praha" style="margin-botom: 1rem; height: 90px"/>
