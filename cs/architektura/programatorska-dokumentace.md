---
layout: default
title: Programátorská dokumentace
nav_order: 8
description: "Odkazy na repozitáře, popis přispívání."
lang-ref: programming-documentation
lang: cs
parent: Architektura
---

# Programátorská dokumentace

Centrální repozitář projektu najdete na [gitlab.com/operator-ict/golemio/](//gitlab.com/operator-ict/golemio/)

Všechny repozitáře s kódy automaticky generují SW dokumentaci ke svým třídám, funkcím, rozhraním. Pro generování této programátorské dokumentace používáme [TypeDoc](//typedoc.org/)

Pro veškeré verzování všech součástí projektu striktně dodržujeme [semantické verzování (semver)](https://semver.org/).


## Repozitář této dokumentace

Tato dokumentace na [gitlab.com/operator-ict/golemio/documentation](//gitlab.com/operator-ict/golemio/documentation)

## Code Contribution

Neváhejte přispět do projektů nebo jakékoliv dokumentace. Co se týče kódu, dodržujte prosím [contribution guidelines](https://gitlab.com/operator-ict/golemio/code/output-gateway/blob/development/CONTRIBUTING.md). Nové změny navrhujte přes [merge requesty](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).
