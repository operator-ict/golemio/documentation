---
layout: default
title: Architektura systému
nav_order: 2
description: "Architektura systému Datové platformy."
parent: Architektura
lang-ref: system-architecture
lang: cs
---

# Architektura systému

Datová platforma je realizována jako modulární systém s využitím obsahující jednotlivě funkční, jednotlivě nasaditelné a nahraditelné moduly. Jednotlivé moduly a vrstvy jsou navrženy s myšlenkou nahraditelnosti za jiné řešení, či již existující službu (např. poskytovanou jako SaaS) a s myšlenkou maximální flexibility a škálovatelnosti.

### Škálovatelnost a modularita

Z důvodu obtížně předvídatelného rozvoje do budoucna (co se týče datových objemů, počtů poskytovatelů dat a jednotlivých use casů) je kladen velký důraz na škálovatelnost - např. vrstva pro příjem dat je navržena jako minimální (lightweight) bezstavová aplikace, kterou je možné v kombinaci s load balancerem nasadit v několika instancích pro rychlý příjem dat a zajištění zařazení požadavků do fronty pro postupné zpracování. Fronta je také integrální součástí systému a realizuje příjem, persistenci dat, distribuci a synchronizaci mezi jednotlivými výpočetními uzly. Systém je tak schopen reagovat i na velké špičky v datových tocích.

### Bezstavové aplikace

Maximum modulů je řešeno jako bezstavové aplikace, kde držitelem stavu je společný databázový cluster. Výpočetní uzly (jednotlivé instance realizující zpracování dat, transformace a výpočty dat) jsou také libovolně škálovatelné a navýšením počtu jejich instancí se urychlí proces zpracování a vyzvedávání dat z fronty.

### Microservice-oriented

Architektura systému kloubí výhody microservice-oriented architektury (flexibilita, samostatné škálování, nahraditelnost, rychlost roll outů nových aktualizací) s tradičním návrhem. To samozřejmě přináší nejen výhody, ale i několik úskalí, na které musel být brán při návrhu a realizaci (zejména z pohledu provozu a povyšování verzí se zpětně nekompatibilními úpravami), jelikož se u některých modulů nejedná čistě o úplně nezávislé microservices.


### CI/CD

Architektura je navržena s myšlenkou kontinuálního nasazování (CI/CD), kde mohou být nové verze jednotlivých services v automatizovaném systému nasazovány průběžně v krátkých iteracích. Platforma je postavena nad cloudovým prostředím s využitím microservices, kontejnerizace, škálování prostředí a využití služeb "as a service".

### Aplikace je rozdělena do jednotlivých samostatných služeb:

* Vstupní rozhraní (Input Gateway),
* Přístupová vrstva (ACL, Access Proxy),
* Fronta (Message Broker, Queue),
* Integrační vrstva (Integration Engine),
* Databázová vrstva,
* Výstupní rozhraní (Output Gateway),
* Správa časově řízených úkonů (CRON).


## Technologické schéma

Následující schéma popisuje technologický stack projektu a využití technologií na jednotlivých součástech platformy.

<img src="{{ site.baseurl }}/assets/images/schemas/Technology_schema.png" alt="Technologické Schéma architektury" class="img-highlighted"/>

Základní vlastnosti architektury systému:

- jednotlivé komponenty mají jasně definované rozhraní a lze je v budoucnu škálovat či nahradit jiným řešením
- všechny příchozí požadavky (requesty) musí projít skrze `Access proxy`, což je vrstva zajištující authentizaci a authorizaci
- Input gateway definuje příchozí endpointy a validuje datovou strukturu příchozích dat
- zpracování dat probíhá přes message queue RabbitMQ pomocí `workeru` v `integračním enginu`
  - flow workeru: vyzvednu zprávu -> provedu můj úkol -> buď uloží výsledek skrze zprávu do další fronty a nebo se jen ukončí
-  data je možné stahovat z externích zdrojů na základě času (`cron`) nebo jiné události, která uloží zprávu do fronty

## Infrastrukturní schéma

Celý projekt běží na Kubernetes a jednotlivé komponenty jsou Docker kontejnery. Více v repositáři [Infrastructure](https://gitlab.com/operator-ict/golemio/devops/infrastructure)


