---
layout: default
title: Architektura
nav_order: 2
description: "Dokumentace platformní architektury"
lang-ref: architecture
lang: cs
has_children: true
---

# Architektura platformy

<img src="{{ site.baseurl }}/assets/images/schemas/DP_schema_general.svg" alt="Schéma architektury" class="img-highlighted"/>

Níže v tabulce je seznam všech informací sloužící k popisu architektury a návrhu platformy ze všech možných úhlů pohledu.

## Seznam dokumentace

| Název | Popis |
|:-|:-|
| [Architektura systému]({% link cs/architektura/architektura-systemu.md %}) | Popis celkové architektury aplikace s vazbami na navazující systémy. Dokumentace obsahuje schémata pro jednotlivé popisované oblasti v rozsahu standardní architektonické dokumentace. |
| [Programátorská dokumentace]({% link cs/architektura/programatorska-dokumentace.md %}) | Použité třídy, interface, struktury, moduly aplikace s popisem funkcionality a s možností využití této dokumentace pro využití stávající funkcionality při rozšíření aplikace. |
