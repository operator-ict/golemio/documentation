---
layout: default
title: Integrace datové sady
lang-ref: integration-of-dataset
lang: cs
nav_order: 2
parent: Tutorialy
---

# Integrace datové sady

<blockquote class="info">
Hlavní návod popisující postup integrace nové datové sady: od analýzy, přes implementaci příjmu PUSH dat/periodické stahování PULL dat, transformaci a uložení do databáze, po zveřejnění a vystavení výstupního API nad daty.
</blockquote>

## 1. Analýza datové sady
- proces získání dat (PULL, PUSH)
- typ a velikost dat
- formát uložení a cílová DB (mongo, postgresql)

## 2. Vytvoření schémat pro dataset (Schema Definitions)
- git repo: [gitlab.com/operator-ict/golemio/code/schema-definitions-public](//gitlab.com/operator-ict/golemio/code/schema-definitions-public)
- vytvoření schémat podle povahy dat (input nebo datasource schéma, output schéma, history schéma, atd.)
- vytvoření migrace DB (nové tabulky, indexy)


## 3. Input Gateway
- git repo: https://gitlab.com/operator-ict/golemio/code/input-gateway
- tento krok pouze pokud jsou data posílána aktivně ze zdroje (PUSH)
- vytvoření endpointu pro příjem dat
- validace příchozích dat
- odeslání dat do fronty
- dokumentace (OpenAPI)

## 4. Integration Engine
- git repo: [gitlab.com/operator-ict/golemio/code/integration-engine](//gitlab.com/operator-ict/golemio/code/integration-engine)
- vytvoření transformace dat, např. `modules/NewDataset/NewDatasetTransformation.ts`
- vytvoření workera, např. `modules/NewDataset/NewDatasetWorker.ts`
- přidání záznamu do `queueDefinitions.ts`
- nadefinování datového zdroje ve workeru, pouze pokud je data nutné aktivně stahovat (PULL)
- nadefinování modelu ve workeru
- samotná implementace metod pro zpracování zpráv z front, celá logika
- napsání testů
- dokumentace (`docs/datasets.md`)

## 5. Definice cron tasku
- git repo: [gitlab.com/operator-ict/golemio/code/cron-tasks](//gitlab.com/operator-ict/golemio/code/cron-tasks)
- pouze pokud je data nutné aktivně stahovat (PULL)
- definovat periodicitu a frontu pro odesílání zpráv z cronu
- odeslat definici na DevOps


## 6. Output Gateway
- git repo: [gitlab.com/operator-ict/golemio/code/output-gateway](//gitlab.com/operator-ict/golemio/code/output-gateway)
- vytvoření rout pro dataset
- definice všech filtru, limitů, atd. podle povahy dat
- definice obohacení (linkování) dat, podle povahy dat
- dokumentace (OpenAPI)
