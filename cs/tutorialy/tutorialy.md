---
layout: default
title: Tutorialy
nav_order: 7
description: "Tutorialy k použití platformy."
has_children: true
lang-ref: tutorials
lang: cs
---

# Tutorialy

Zde je soubor všech návodů pro konfiguraci existujicích datových sad popř. integraci nových, návody ke spuštění projektu a jiné obecné tutoriály. V případě dotazů nám napište na [golemio@operatorict.cz](mailto:golemio@operatorict.cz)
