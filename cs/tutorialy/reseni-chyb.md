---
layout: default
title: Řešení chyb
lang-ref: troubleshooting
lang: cs
nav_order: 3
parent: Tutorialy
---

# Řešení chyb

## Případy chyb systému a jejich řešení

### Chyba v datovém zdroji

PUSH data - případ výpadku externího systému, který nám aktivně posílá data nemáme příliš možností spolehlivě poznat (systém buď vypadl, přestal nám posílat data, zrovna žádná data neposlal nebo došlo k přerušení nebo zpoždění na síti). Pro tento případ máme realtime monitoring množství přijatých dat a alerting na časové okno bez dat. V případě takového alertu můžeme náš zdroj upozornit na výpadek v datech a předat k řešení na jejich straně.

PULL data - máme nastavený alerting na chybu dovolání se externího API nebo validace dat. V případě chyby řešíme s dodavatelem (nefunkční, nedostupné API, chybná data). Ukládáme log všech zaznamenaných chyb do databáze pro celkový pohled a případné dohledání historie nebo doplnění zveřejněné datové sady (pokud publikujeme datovou sadu jako OpenData, přidáme informaci o chybách/nedostupnosti dat, aby bylo uživatelům dat jasno, kde data chybí, kde je "mezera v datech" a jaká byla příčina, aby s tím mohli počítat v případě statistických výpočtů apod.).

## Chyby v rámci jednotlivých modulů

### Input Gateway

Logování přijatých dat a jejich počtu do InfluxDB + custom alerty na treshold a časy + normální error logy

### Integration Engine

Custom errory do PostgreSQL, počty záznamů do InfluxDB.

Podle statusu/severity chyby, zda je ERROR/WARNING posílá Integration Engine zprávu zpět do RabbitMQ (nAck), nebo potvrdí její převzetí a zpracování s nekritickou chybou (Ack).

V RabbitMQ jsou nepřijaté, odmítnuté, nezpracované zprávy ukládány do "dead queue", kde máme nastavený monitoring na jejich počet a možnost jejich manuálního zpracování/přehození do jiné fronty. V Dead Queue končí všechny zprávy které se nezpracují (skončí chybou severity `error`, neznámou chybou, nebo neskončí vůbec - vyprší jejich TTL).

Máme sepsané kódy a významy všech chyb. Na všechny chyby je nastaven alerting. Pokud identifikujeme nějaké chyby, které nemají vliv na běh systému nebo jsou "běžné", explicitně u těchto známých chyb snižujeme jejich severitu na `warning`. Výpis všech chyb níže.

#### Connection Errors

`1001`: Error while connecting to {name}.

`1002`: {name} connection not exists. First call connect() method.

`1003`: Sending the message to exchange failed.

`1004`: Error while saving data to InfluxDB.


#### Datasources Errors

`2001`: Retrieving of the source data failed.

`2002`: Error while getting data from server.

`2003`: Error while parsing source data.

`2004`: Error while validating source data.


#### Transformations Errors

`3001`: Sorted Waste Containers were not set. Use method setContainers().

`3002`: {name} must be a valid number.


#### Models Errors

`4001`: Error while saving to database.

`4002`: Error while truncating data.

`4003`: Model data was not found.

`4004`: Error while getting from database.

`4005`: Error while validating data.


#### Workers Errors

`5001`: Error while updating {name}.

```
Chyba `5001` je zpracovávána jako `warning`
```

`5002`: Error while purging old data.

`5003`: Error while sending the POST request.

`5004`: Error while checking RopidGTFS saved rows.

`5005`: Worker and worker method must be defined.


#### Other Errors

`6001`: Method is not implemented.

`6002`: Retrieving of the open street map nominatim data failed.
```
Chyba `6002` je zpracovávána jako `warning`
```

`6003`: The ENV variable {name} cannot be undefined.

### Output Gateway

Všechny výstupní API vrací standardní [HTTP status kódy](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status). Implementace v balíčku [@golemio/errors](https://gitlab.com/operator-ict/golemio/code/errors/blob/development/src/HTTPErrorHandler.ts). Standardní možné chyby v odpovědích z API jsou součástí OpenAPI specifikace - 404 Not Found, 401 Not Authorized, 403 Forbidden, 429 Too Many Requests, apod.

### Errory v kódu (JS)

V rámci JS kódu v celém projektu používáme centralizované zpracování chyb, používáme vlastní balíček [@golemio/errors](https://www.npmjs.com/package/@golemio/errors), který rozšiřuje nativní JavaScript `Error`, ale umožňuje nám v rámci jeho odchycení zachytit mnohem více informací a poskytuje další funkcionalitu. Rozlišujeme mezi `operativními` a `neoperativními` chybami - chyba programu/programátora vs. očekávaná chyba v aplikaci, kterou aplikace umí zpracovat. Zachycujeme celý stack-trace (ale neposíláme uživateli), známe původ chyby.

Balíček [@golemio/errors](https://www.npmjs.com/package/@golemio/errors) je snadno univerzálně použitelný i pro jiné projekty.