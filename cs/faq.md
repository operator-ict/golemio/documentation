---
layout: default
title: FAQ
nav_order: 6
description: "Časté otázky k Datové platformě Golemio."
lang-ref: faq
lang: cs
---

# FAQ

> Zde je soubor nejčastějších dotazů k Datové platformě, pokud tento článek nezodpoví na vaše otázky, napište nám na [golemio@operatorict.cz](mailto:golemio@operatorict.cz)


## Obecné

### Plní vaše řešení funkci IoT platformy ve smyslu sběru dat ze SigFox, Lora a dalších sítí?

Nikoliv. V Praze jsme se dosud setkávali v projektech vždy s dodavateli poskytující standardní REST API skrze HTTP, není ani na pořadu dne ji využívat. Nicméně díky modularní architektuře platformy je možné kdykoliv předřadit existující řešení poskytující funkci IoT platformy pro komunikaci na odpovídajících IoT sítích.

### Je celá Datová platforma Open Source?

Ano, od roku 2019 zveřejnujeme zdrojové kódy.

### Je připraven software k použití i pro nasazení v jiných městech?

Díky modulárnímu systémů je možné nasadit datovou platfomu i v jiném městě/kraji, ale bude to vždy vyžadovat práci programátora pro nastavení dle daných potřeb.

### Je to zadarmo?

Publikovali jsme kódy pod licencí MIT, odpověď tedy zní ano, kódy můžete vzít a používat i pro komerční účely zadarmo v rámci podmínek této volné licence. Chcete-li si přečíst o tom, co MIT obnáší, doporučujeme článek na [choosealicense.com/licenses/mit/](//choosealicense.com/licenses/mit/).



## Architektura

### Máte bezstavové server aplikace, kde si udržujete stav?

V databázi a message brokeru(RabbitMQ).

### Znamená to, že některé různé moduly jsou závislé na stejné databázi a nejsou to tedy samostatné nezávislé microservices?

Ano. Databázi máme společnou, minimálně pro output-gateway, integration-engine. Permission-proxy a uživatelská data jsou uloženy v samostatné oddělené relační databázi.

### Proč jsou tedy ty aplikace vůbec oddělené?

Zejména kvůli škálovatelnosti a samostatné nasaditelnosti. Pokud upravíme něco na výstupním API, deployneme pouze nové output-gateway. A škálovat můžeme také samostatně, např. vrstvu pro příjem dat (input-gateway), kde můžeme nasadit např. 10x více instancí nezávisle, pokud nám začne chodit větší množství dat.

