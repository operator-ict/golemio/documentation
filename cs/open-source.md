---
layout: default
title: Open Source
nav_order: 9
description: "Publikace zdrojových kódů"
lang-ref: open-source
lang: cs
---

# Open Source

> Zdrojový kód celého projektu zveřejňujeme jako Open Source pod licencí MIT.


## Git

Centrální skupinu repozitářů projektu najdete na [gitlab.com/operator-ict/golemio/](//gitlab.com/operator-ict/golemio/). Tyto repozitáře zároveň slouží i pro nás jako primární zdroj pro provozní prostředí datové platformy Golemio. Co publikujeme, sami používáme. Pokud se tedy do projektu rozhodnete přispět, stačí s navrhovanou změnou vytvořit merge request ve správném formátu a po jeho odsouhlasení a přijetí do repozitáře bude změna automaticky nasazena do (ve výsledku až produkčního) prostředí Datové platformy Golemio. Nezapomeňte si však přečíst [contribution guidelines](https://gitlab.com/operator-ict/golemio/code/modules/core/-/blob/master/CONTRIBUTING.md?ref_type=heads).

Sami ❤ OpenSource a hodně ho využíváme, závisíme na množství volně dostupných balíčků (zejména z ekosystému [npmjs.com](https://www.npmjs.com/)). V rámci projektu i sami publikujeme některé balíčky přímo na NPMjs (k jednoduchému použití jak pro nás, tak případně na ostatních projektech). Všechny naše zvřejněné balíčky můžete najít pod scope [@golemio](https://www.npmjs.com/search?q=%40golemio).
<br>