---
layout: default
title: Otevřená data & API
nav_order: 8
description: "Informace k publikovaným otevřeným datům a veřejnému API."
lang-ref: open-data-api
lang: cs
---

# Otevřená data & API

> V rámci projektu poskytuje široké veřejnosti data k dalšímu použití jako tzv. Open Data. Data jsou povahou realtime data, statistická data, popř. číselníky, katalogy a mapová data.

## Možnosti přístupu

- Opendata katalog Hlavního města Prahy ([opendata.praha.eu](http://opendata.praha.eu){:target="_blank"})
    - Exporty dat s meta informacemi o struktuře, povaze, zdroji, četnosti aktualizace, kontaktní osobě atd.
    - Stálé URL pro datové sady
    - Katalogizace v rámci NKOD ([data.gov.cz](//data.gov.cz){:target="_blank"})
- Public API - Realtime přístup do Datové platformy Golemio
    - REST API zdokumentované na [api.golemio.cz/docs/public-openapi/](https://api.golemio.cz/docs/public-openapi/){:target="_blank"}
    - Přístup zabezpečený pomocí `JWT`


## Publikovaná data v katalogu OD

Operátor ICT provozuje pro HMP katalog otevřených dat, na data, která zvěřejňuje z projektu Golemio se lze dostat na [opendata.praha.eu/organizations/operator-ict](https://opendata.praha.eu/organizations/operator-ict){:target="_blank"}.

## Přístup k veřejnému API

### API Dokumentace

> [api.golemio.cz/docs/public-openapi/](https://api.golemio.cz/docs/public-openapi/){:target="_blank"}

<br >

Na tomto linku najdete kompletní dokumentaci našeho REST API, vč. popisu jednotlivých endpointů a metod, ukázkových requestů, popisu struktury a ukázek návratových dat, hlaviček requestů a možnosti testovacího (mock/sandbox) serveru nebo generování kódu pro volání API v různých jazycích (cURL, Node.js, Java, Python,...).

### API URL

> https://api.golemio.cz/`version`/`route`


#### Zabezpečení

Využíváme JWT token / vygenerovaný API klíč.

### Registrace uživatele

Na stránce [api.golemio.cz/api-keys](//api.golemio.cz/api-keys){:target="_blank"} si lze po bezplatné registraci a ověření přes váš email vygenerovat `api-key` pro přístup k API.

<!-- ## Dokumentace datových sad

Pro úplnost zde uvádíme i některé z datových sad, u kterých je vhodné rozšířit dokumentaci nad rámec dokumentace v API Blueprint. Kliknutím na příslušnou podkategorii přejděte na detailní popis. -->
